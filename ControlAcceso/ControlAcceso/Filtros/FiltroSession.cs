﻿using ControlAcceso.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlAcceso.Filtros
{
    //Primero heredamos de la clase IActionFilter, que nos proporcionará dos métodos
    public class FiltroSession : IActionFilter
    {
        //Añadimos el _context, para poder usarlo en expresiones landa
        private readonly ControlContext _context;

        public FiltroSession(ControlContext context)
        {
            _context = context;
        }
        public void OnActionExecuted(ActionExecutedContext context)
        {
            //deberemos comentar el código o borrarlo para que no cause errores
        }
        //Este será el método que normalmente usaremos
        public void OnActionExecuting(ActionExecutingContext context)
        {
            // Recuperamos el usuario a través de la id que tenemos en sesion
            var user = _context.UserAccounts.Where(u => u.ID == Convert.ToInt32(context.HttpContext.Session.GetString("usuario"))).FirstOrDefault();
            //if(user==null) si no existe el usuario
            if(user == null)
            {
                context.Result = new RedirectToActionResult("index", "login", null);
            }
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ControlAcceso.Migrations
{
    public partial class m4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "UserAccount",
                keyColumn: "ID",
                keyValue: 1,
                column: "Password",
                value: "MQAyADMANAA=");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "UserAccount",
                keyColumn: "ID",
                keyValue: 1,
                column: "Password",
                value: "1234");
        }
    }
}

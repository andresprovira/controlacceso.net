﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ControlAcceso.Migrations
{
    public partial class m6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Admin",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "UserAccount",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "ID",
                keyValue: 1);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "ID", "Enabled", "RoleDescription", "RoleName" },
                values: new object[] { 1, false, "jefe", "Admin" });

            migrationBuilder.InsertData(
                table: "UserAccount",
                columns: new[] { "ID", "Active", "Email", "Password", "RoleID", "Username" },
                values: new object[] { 1, true, "pepe@email.es", "MQAyADMANAA=", 1, "pepito" });

            migrationBuilder.InsertData(
                table: "Admin",
                columns: new[] { "ID", "Age", "Name", "Phone", "Surname", "UserAccountID" },
                values: new object[] { 1, 28, "Pepe", "12345678", "Perez", 1 });
        }
    }
}

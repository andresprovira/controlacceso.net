﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ControlAcceso.Migrations
{
    public partial class _11 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Menu",
                columns: new[] { "ID", "Action", "Controller", "Label" },
                values: new object[,]
                {
                    { 1, "Index", "UserAccounts", "UserAccounts" },
                    { 2, "Index", "Roles", "Roles" },
                    { 3, "Index", "Menus", "Menus" },
                    { 4, "Index", "Horses", "Horses" },
                    { 5, "Index", "HorseSaddles", "HorseSaddles" },
                    { 6, "Index", "RaceTracks", "RaceTracks" },
                    { 7, "Index", "Bookings", "Bookings" },
                    { 8, "Index", "RoleHasMenus", "RoleHasMenus" },
                    { 9, "Index", "HorseClasses", "HorseClasses" },
                    { 10, "Index", "Customers", "Customers" },
                    { 11, "Index", "Teachers", "Teachers" },
                    { 12, "Index", "Admins", "Admins" }
                });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "ID", "Enabled", "RoleDescription", "RoleName" },
                values: new object[,]
                {
                    { 1, false, "Rol de Admin", "Admin" },
                    { 2, false, "Rol de Teacher", "Teacher" },
                    { 3, false, "Rol de Customer", "Customer" }
                });

            migrationBuilder.InsertData(
                table: "RoleHasMenu",
                columns: new[] { "ID", "MenuID", "RoleID" },
                values: new object[,]
                {
                    { 1, 1, 1 },
                    { 16, 9, 2 },
                    { 15, 6, 2 },
                    { 14, 5, 2 },
                    { 13, 4, 2 },
                    { 12, 12, 1 },
                    { 11, 11, 1 },
                    { 17, 7, 3 },
                    { 10, 10, 1 },
                    { 8, 8, 1 },
                    { 7, 7, 1 },
                    { 6, 6, 1 },
                    { 5, 5, 1 },
                    { 4, 4, 1 },
                    { 3, 3, 1 },
                    { 2, 2, 1 },
                    { 9, 9, 1 }
                });

            migrationBuilder.InsertData(
                table: "UserAccount",
                columns: new[] { "ID", "Active", "Email", "Password", "RoleID", "Username" },
                values: new object[,]
                {
                    { 1, true, "admin@email.es", "MQAyADMANAA=", 1, "admin" },
                    { 2, true, "teacher@email.es", "MQAyADMANAA=", 2, "teacher" },
                    { 3, true, "customer@email.es", "MQAyADMANAA=", 3, "customer" }
                });

            migrationBuilder.InsertData(
                table: "Admin",
                columns: new[] { "ID", "Age", "Name", "Phone", "Surname", "UserAccountID" },
                values: new object[] { 1, 30, "Admin", "123456789", "Admin", 1 });

            migrationBuilder.InsertData(
                table: "Customer",
                columns: new[] { "ID", "Address", "Age", "Birthday", "City", "DNI", "Level", "Name", "Phone", "PostalCode", "Surname", "UserAccountID" },
                values: new object[] { 1, "Calle de ejemplo", 40, new DateTime(2022, 2, 9, 11, 34, 54, 776, DateTimeKind.Local).AddTicks(9659), "Sevilla", "12345678V", "1", "Customer", "123456789", "12345", "Customer", 3 });

            migrationBuilder.InsertData(
                table: "Teacher",
                columns: new[] { "ID", "Age", "Name", "Phone", "Surname", "UserAccountID" },
                values: new object[] { 1, 35, "Teacher", "123456789", "Teacher", 2 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Admin",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Customer",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "RoleHasMenu",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "RoleHasMenu",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "RoleHasMenu",
                keyColumn: "ID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "RoleHasMenu",
                keyColumn: "ID",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "RoleHasMenu",
                keyColumn: "ID",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "RoleHasMenu",
                keyColumn: "ID",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "RoleHasMenu",
                keyColumn: "ID",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "RoleHasMenu",
                keyColumn: "ID",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "RoleHasMenu",
                keyColumn: "ID",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "RoleHasMenu",
                keyColumn: "ID",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "RoleHasMenu",
                keyColumn: "ID",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "RoleHasMenu",
                keyColumn: "ID",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "RoleHasMenu",
                keyColumn: "ID",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "RoleHasMenu",
                keyColumn: "ID",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "RoleHasMenu",
                keyColumn: "ID",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "RoleHasMenu",
                keyColumn: "ID",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "RoleHasMenu",
                keyColumn: "ID",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Teacher",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Menu",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Menu",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Menu",
                keyColumn: "ID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Menu",
                keyColumn: "ID",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Menu",
                keyColumn: "ID",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Menu",
                keyColumn: "ID",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Menu",
                keyColumn: "ID",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Menu",
                keyColumn: "ID",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Menu",
                keyColumn: "ID",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Menu",
                keyColumn: "ID",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Menu",
                keyColumn: "ID",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Menu",
                keyColumn: "ID",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "UserAccount",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "UserAccount",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "UserAccount",
                keyColumn: "ID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "ID",
                keyValue: 3);
        }
    }
}

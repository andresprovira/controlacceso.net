﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ControlAcceso.Migrations
{
    public partial class _12 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Customer",
                keyColumn: "ID",
                keyValue: 1,
                column: "Birthday",
                value: new DateTime(2022, 2, 9, 11, 36, 8, 86, DateTimeKind.Local).AddTicks(2140));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Customer",
                keyColumn: "ID",
                keyValue: 1,
                column: "Birthday",
                value: new DateTime(2022, 2, 9, 11, 34, 54, 776, DateTimeKind.Local).AddTicks(9659));
        }
    }
}

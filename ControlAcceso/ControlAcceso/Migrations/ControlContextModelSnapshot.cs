﻿// <auto-generated />
using System;
using ControlAcceso.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace ControlAcceso.Migrations
{
    [DbContext(typeof(ControlContext))]
    partial class ControlContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.12")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ControlAcceso.Models.Admin", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("Age")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(20)
                        .HasColumnType("nvarchar(20)");

                    b.Property<string>("Phone")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Surname")
                        .IsRequired()
                        .HasMaxLength(20)
                        .HasColumnType("nvarchar(20)");

                    b.Property<int>("UserAccountID")
                        .HasColumnType("int");

                    b.HasKey("ID");

                    b.HasIndex("UserAccountID");

                    b.ToTable("Admin");

                    b.HasData(
                        new
                        {
                            ID = 1,
                            Age = 30,
                            Name = "Admin",
                            Phone = "123456789",
                            Surname = "Admin",
                            UserAccountID = 1
                        });
                });

            modelBuilder.Entity("ControlAcceso.Models.Booking", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("BookingDate")
                        .HasColumnType("datetime2");

                    b.Property<int>("CustomerID")
                        .HasColumnType("int");

                    b.Property<int>("HorseClassID")
                        .HasColumnType("int");

                    b.Property<int>("HorseID")
                        .HasColumnType("int");

                    b.HasKey("ID");

                    b.HasIndex("CustomerID");

                    b.HasIndex("HorseClassID");

                    b.HasIndex("HorseID");

                    b.ToTable("Booking");
                });

            modelBuilder.Entity("ControlAcceso.Models.Customer", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Address")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Age")
                        .HasColumnType("int");

                    b.Property<DateTime>("Birthday")
                        .HasColumnType("datetime2");

                    b.Property<string>("City")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("DNI")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Level")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(20)
                        .HasColumnType("nvarchar(20)");

                    b.Property<string>("Phone")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PostalCode")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Surname")
                        .IsRequired()
                        .HasMaxLength(20)
                        .HasColumnType("nvarchar(20)");

                    b.Property<int>("UserAccountID")
                        .HasColumnType("int");

                    b.HasKey("ID");

                    b.HasIndex("UserAccountID");

                    b.ToTable("Customer");

                    b.HasData(
                        new
                        {
                            ID = 1,
                            Address = "Calle de ejemplo",
                            Age = 40,
                            Birthday = new DateTime(2022, 2, 9, 11, 36, 8, 86, DateTimeKind.Local).AddTicks(2140),
                            City = "Sevilla",
                            DNI = "12345678V",
                            Level = "1",
                            Name = "Customer",
                            Phone = "123456789",
                            PostalCode = "12345",
                            Surname = "Customer",
                            UserAccountID = 3
                        });
                });

            modelBuilder.Entity("ControlAcceso.Models.Horse", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("Birthday")
                        .HasColumnType("datetime2");

                    b.Property<string>("Colour")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("HorseSaddleID")
                        .HasColumnType("int");

                    b.Property<byte[]>("Image")
                        .HasColumnType("varbinary(max)");

                    b.Property<string>("ImageContentType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ImageSourceFileName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Level")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Race")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("ID");

                    b.HasIndex("HorseSaddleID");

                    b.ToTable("Horse");
                });

            modelBuilder.Entity("ControlAcceso.Models.HorseClass", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("Assistants")
                        .HasColumnType("int");

                    b.Property<DateTime>("Date")
                        .HasColumnType("datetime2");

                    b.Property<int>("Duration")
                        .HasColumnType("int");

                    b.Property<DateTime>("Hour")
                        .HasColumnType("datetime2");

                    b.Property<string>("Level")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("RaceTrackID")
                        .HasColumnType("int");

                    b.Property<int>("TeacherID")
                        .HasColumnType("int");

                    b.HasKey("ID");

                    b.HasIndex("RaceTrackID");

                    b.HasIndex("TeacherID");

                    b.ToTable("HorseClass");
                });

            modelBuilder.Entity("ControlAcceso.Models.HorseSaddle", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Condition")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("Reserved")
                        .HasColumnType("bit");

                    b.HasKey("ID");

                    b.ToTable("HorseSaddle");
                });

            modelBuilder.Entity("ControlAcceso.Models.Menu", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Action")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Controller")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Label")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("ID");

                    b.ToTable("Menu");

                    b.HasData(
                        new
                        {
                            ID = 1,
                            Action = "Index",
                            Controller = "UserAccounts",
                            Label = "UserAccounts"
                        },
                        new
                        {
                            ID = 2,
                            Action = "Index",
                            Controller = "Roles",
                            Label = "Roles"
                        },
                        new
                        {
                            ID = 3,
                            Action = "Index",
                            Controller = "Menus",
                            Label = "Menus"
                        },
                        new
                        {
                            ID = 4,
                            Action = "Index",
                            Controller = "Horses",
                            Label = "Horses"
                        },
                        new
                        {
                            ID = 5,
                            Action = "Index",
                            Controller = "HorseSaddles",
                            Label = "HorseSaddles"
                        },
                        new
                        {
                            ID = 6,
                            Action = "Index",
                            Controller = "RaceTracks",
                            Label = "RaceTracks"
                        },
                        new
                        {
                            ID = 7,
                            Action = "Index",
                            Controller = "Bookings",
                            Label = "Bookings"
                        },
                        new
                        {
                            ID = 8,
                            Action = "Index",
                            Controller = "RoleHasMenus",
                            Label = "RoleHasMenus"
                        },
                        new
                        {
                            ID = 9,
                            Action = "Index",
                            Controller = "HorseClasses",
                            Label = "HorseClasses"
                        },
                        new
                        {
                            ID = 10,
                            Action = "Index",
                            Controller = "Customers",
                            Label = "Customers"
                        },
                        new
                        {
                            ID = 11,
                            Action = "Index",
                            Controller = "Teachers",
                            Label = "Teachers"
                        },
                        new
                        {
                            ID = 12,
                            Action = "Index",
                            Controller = "Admins",
                            Label = "Admins"
                        });
                });

            modelBuilder.Entity("ControlAcceso.Models.RaceTrack", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("ID");

                    b.ToTable("RaceTrack");
                });

            modelBuilder.Entity("ControlAcceso.Models.Role", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<bool>("Enabled")
                        .HasColumnType("bit");

                    b.Property<string>("RoleDescription")
                        .IsRequired()
                        .HasMaxLength(100)
                        .HasColumnType("nvarchar(100)");

                    b.Property<string>("RoleName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("ID");

                    b.ToTable("Role");

                    b.HasData(
                        new
                        {
                            ID = 1,
                            Enabled = false,
                            RoleDescription = "Rol de Admin",
                            RoleName = "Admin"
                        },
                        new
                        {
                            ID = 2,
                            Enabled = false,
                            RoleDescription = "Rol de Teacher",
                            RoleName = "Teacher"
                        },
                        new
                        {
                            ID = 3,
                            Enabled = false,
                            RoleDescription = "Rol de Customer",
                            RoleName = "Customer"
                        });
                });

            modelBuilder.Entity("ControlAcceso.Models.RoleHasMenu", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("MenuID")
                        .HasColumnType("int");

                    b.Property<int>("RoleID")
                        .HasColumnType("int");

                    b.HasKey("ID");

                    b.HasIndex("MenuID");

                    b.HasIndex("RoleID");

                    b.ToTable("RoleHasMenu");

                    b.HasData(
                        new
                        {
                            ID = 1,
                            MenuID = 1,
                            RoleID = 1
                        },
                        new
                        {
                            ID = 2,
                            MenuID = 2,
                            RoleID = 1
                        },
                        new
                        {
                            ID = 3,
                            MenuID = 3,
                            RoleID = 1
                        },
                        new
                        {
                            ID = 4,
                            MenuID = 4,
                            RoleID = 1
                        },
                        new
                        {
                            ID = 5,
                            MenuID = 5,
                            RoleID = 1
                        },
                        new
                        {
                            ID = 6,
                            MenuID = 6,
                            RoleID = 1
                        },
                        new
                        {
                            ID = 7,
                            MenuID = 7,
                            RoleID = 1
                        },
                        new
                        {
                            ID = 8,
                            MenuID = 8,
                            RoleID = 1
                        },
                        new
                        {
                            ID = 9,
                            MenuID = 9,
                            RoleID = 1
                        },
                        new
                        {
                            ID = 10,
                            MenuID = 10,
                            RoleID = 1
                        },
                        new
                        {
                            ID = 11,
                            MenuID = 11,
                            RoleID = 1
                        },
                        new
                        {
                            ID = 12,
                            MenuID = 12,
                            RoleID = 1
                        },
                        new
                        {
                            ID = 13,
                            MenuID = 4,
                            RoleID = 2
                        },
                        new
                        {
                            ID = 14,
                            MenuID = 5,
                            RoleID = 2
                        },
                        new
                        {
                            ID = 15,
                            MenuID = 6,
                            RoleID = 2
                        },
                        new
                        {
                            ID = 16,
                            MenuID = 9,
                            RoleID = 2
                        },
                        new
                        {
                            ID = 17,
                            MenuID = 7,
                            RoleID = 3
                        });
                });

            modelBuilder.Entity("ControlAcceso.Models.Teacher", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("Age")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(20)
                        .HasColumnType("nvarchar(20)");

                    b.Property<string>("Phone")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Surname")
                        .IsRequired()
                        .HasMaxLength(20)
                        .HasColumnType("nvarchar(20)");

                    b.Property<int>("UserAccountID")
                        .HasColumnType("int");

                    b.HasKey("ID");

                    b.HasIndex("UserAccountID");

                    b.ToTable("Teacher");

                    b.HasData(
                        new
                        {
                            ID = 1,
                            Age = 35,
                            Name = "Teacher",
                            Phone = "123456789",
                            Surname = "Teacher",
                            UserAccountID = 2
                        });
                });

            modelBuilder.Entity("ControlAcceso.Models.UserAccount", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<bool>("Active")
                        .HasColumnType("bit");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Password")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("RoleID")
                        .HasColumnType("int");

                    b.Property<string>("Username")
                        .IsRequired()
                        .HasMaxLength(20)
                        .HasColumnType("nvarchar(20)");

                    b.HasKey("ID");

                    b.HasIndex("RoleID");

                    b.ToTable("UserAccount");

                    b.HasData(
                        new
                        {
                            ID = 1,
                            Active = true,
                            Email = "admin@email.es",
                            Password = "MQAyADMANAA=",
                            RoleID = 1,
                            Username = "admin"
                        },
                        new
                        {
                            ID = 2,
                            Active = true,
                            Email = "teacher@email.es",
                            Password = "MQAyADMANAA=",
                            RoleID = 2,
                            Username = "teacher"
                        },
                        new
                        {
                            ID = 3,
                            Active = true,
                            Email = "customer@email.es",
                            Password = "MQAyADMANAA=",
                            RoleID = 3,
                            Username = "customer"
                        });
                });

            modelBuilder.Entity("ControlAcceso.Models.Admin", b =>
                {
                    b.HasOne("ControlAcceso.Models.UserAccount", "UserAccount")
                        .WithMany()
                        .HasForeignKey("UserAccountID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("UserAccount");
                });

            modelBuilder.Entity("ControlAcceso.Models.Booking", b =>
                {
                    b.HasOne("ControlAcceso.Models.Customer", "Customer")
                        .WithMany("ListBookings")
                        .HasForeignKey("CustomerID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("ControlAcceso.Models.HorseClass", "HorseClass")
                        .WithMany("ListBookings")
                        .HasForeignKey("HorseClassID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("ControlAcceso.Models.Horse", "Horse")
                        .WithMany("ListBookings")
                        .HasForeignKey("HorseID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Customer");

                    b.Navigation("Horse");

                    b.Navigation("HorseClass");
                });

            modelBuilder.Entity("ControlAcceso.Models.Customer", b =>
                {
                    b.HasOne("ControlAcceso.Models.UserAccount", "UserAccount")
                        .WithMany()
                        .HasForeignKey("UserAccountID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("UserAccount");
                });

            modelBuilder.Entity("ControlAcceso.Models.Horse", b =>
                {
                    b.HasOne("ControlAcceso.Models.HorseSaddle", "HorseSaddle")
                        .WithMany()
                        .HasForeignKey("HorseSaddleID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("HorseSaddle");
                });

            modelBuilder.Entity("ControlAcceso.Models.HorseClass", b =>
                {
                    b.HasOne("ControlAcceso.Models.RaceTrack", "RaceTrack")
                        .WithMany("ListHorseClass")
                        .HasForeignKey("RaceTrackID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("ControlAcceso.Models.Teacher", "Teacher")
                        .WithMany("ListHorseClass")
                        .HasForeignKey("TeacherID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("RaceTrack");

                    b.Navigation("Teacher");
                });

            modelBuilder.Entity("ControlAcceso.Models.RoleHasMenu", b =>
                {
                    b.HasOne("ControlAcceso.Models.Menu", "Menu")
                        .WithMany("ListRoleHasMenu")
                        .HasForeignKey("MenuID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("ControlAcceso.Models.Role", "Role")
                        .WithMany("ListRolesHasMenu")
                        .HasForeignKey("RoleID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Menu");

                    b.Navigation("Role");
                });

            modelBuilder.Entity("ControlAcceso.Models.Teacher", b =>
                {
                    b.HasOne("ControlAcceso.Models.UserAccount", "UserAccount")
                        .WithMany()
                        .HasForeignKey("UserAccountID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("UserAccount");
                });

            modelBuilder.Entity("ControlAcceso.Models.UserAccount", b =>
                {
                    b.HasOne("ControlAcceso.Models.Role", "Role")
                        .WithMany("ListUserAccounts")
                        .HasForeignKey("RoleID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Role");
                });

            modelBuilder.Entity("ControlAcceso.Models.Customer", b =>
                {
                    b.Navigation("ListBookings");
                });

            modelBuilder.Entity("ControlAcceso.Models.Horse", b =>
                {
                    b.Navigation("ListBookings");
                });

            modelBuilder.Entity("ControlAcceso.Models.HorseClass", b =>
                {
                    b.Navigation("ListBookings");
                });

            modelBuilder.Entity("ControlAcceso.Models.Menu", b =>
                {
                    b.Navigation("ListRoleHasMenu");
                });

            modelBuilder.Entity("ControlAcceso.Models.RaceTrack", b =>
                {
                    b.Navigation("ListHorseClass");
                });

            modelBuilder.Entity("ControlAcceso.Models.Role", b =>
                {
                    b.Navigation("ListRolesHasMenu");

                    b.Navigation("ListUserAccounts");
                });

            modelBuilder.Entity("ControlAcceso.Models.Teacher", b =>
                {
                    b.Navigation("ListHorseClass");
                });
#pragma warning restore 612, 618
        }
    }
}

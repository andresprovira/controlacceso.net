﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ControlAcceso.Models
{
    public class HorseClass
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Level { get; set; }
        [DataType(DataType.Time)]
        [DisplayName("Hora")]
        public DateTime Hour { get; set; }
        [DataType(DataType.Date)]
        [DisplayName("Fecha")]
        public DateTime Date { get; set; }
        [Required]
        //[Range(5,30)]
        public int Assistants { get; set; }
        [Required]
        [Range(60, 120)]
        public int Duration { get; set; }
        public int TeacherID { get; set; }
        [ForeignKey("TeacherID")]
        public Teacher Teacher { get; set; }
        public int RaceTrackID { get; set; }
        [ForeignKey("RaceTrackID")]
        public RaceTrack RaceTrack { get; set; }
        [InverseProperty("HorseClass")]
        public List<Booking> ListBookings { get; set; }

    }
}
